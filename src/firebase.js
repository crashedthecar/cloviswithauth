// src/firebase.js

import Firebase from 'firebase'
const config = {
    apiKey: "AIzaSyA71YzS7i9UinP7poZtXDbHQ_QwDchL96s",
    authDomain: "doggydog-68464.firebaseapp.com",
    databaseURL: "https://doggydog-68464.firebaseio.com",
    projectId: "doggydog-68464",
    storageBucket: "doggydog-68464.appspot.com",
    messagingSenderId: "687391765546"
  };
const app = Firebase.initializeApp(config);
export const db = app.database();
export const provider = new Firebase.auth.GoogleAuthProvider();
export const auth = Firebase.auth();
